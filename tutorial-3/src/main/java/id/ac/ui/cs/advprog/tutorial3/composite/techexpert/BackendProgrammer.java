package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {
    public BackendProgrammer(String name, Double salary) {
        //TODO Implement
            this.name = name;
            if (salary < 20000.00) {
                throw new IllegalArgumentException();
            } else {
                this.salary = salary;
            }
            this.role = "Back End Programmer";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }

    @Override
    public String getRole() {
        return this.role;
    }
}
