package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    // TODO Complete me!
    ModelDuck(){
        this.setFlyBehavior(
            new FlyRocketPowered()
        );
        this.setQuackBehavior(
            new Squeak()
        );
    }

    @Override
    public void display() {
        System.out.println("This is a Model Duck");
    }
}