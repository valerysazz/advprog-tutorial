package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    MallardDuck(){
        this.setFlyBehavior(
            new FlyNoWay()
        );
        this.setQuackBehavior(
            new Quack()
        );
    }
    
    @Override
    public void display() {
        System.out.println("This is a Mallard Duck");
    }
}
