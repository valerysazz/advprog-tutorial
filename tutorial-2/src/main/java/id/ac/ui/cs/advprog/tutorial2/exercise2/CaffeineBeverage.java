package id.ac.ui.cs.advprog.tutorial2.exercise2;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class CaffeineBeverage {

    public final void prepareRecipe() {
        // TODO Complete me!
        this.boilWater();
        this.brew();
        this.pourInCup();
        this.addCondiments();
    }

    protected abstract void addCondiments();

    protected abstract void brew();

    public void boilWater() {
        System.out.println("Boiling water");
    }

    public void pourInCup() {
        System.out.println("Pouring into cup");
    }

    public boolean customerWantsCondiments() {
        String answer = getUserInput();
            if (answer.toLowerCase().startsWith("y")) {
                return true;
            } else {
                return false;
            }
    }

    private String getUserInput() {
        String answer = null;
        System.out.print("Would you like milk and sugar with your coffee (y/n)? ");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            try {
                answer = in.readLine();
            } catch (IOException ioe) {
                System.err.println("IO error trying to read your answer");
            }        if (answer == null) {
                return "no";
            }
        return answer;
    }
}
