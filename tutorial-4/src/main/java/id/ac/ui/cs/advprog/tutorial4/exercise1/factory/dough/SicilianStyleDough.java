package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class SicilianStyleDough implements Dough {
    public String toString() {
        return "Sicilian Style Dough";
    }
}
