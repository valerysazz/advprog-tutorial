package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class SoftShell implements Clams {

    public String toString() {
        return "Soft-Shell Clams from Lombok";
    }
}
