package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SoftShellTest {

    private SoftShell softShell;

    @Before
    public void setUp() {
        softShell = new SoftShell();
    }

    @Test
    public void testToString() {
        assertEquals("Soft-Shell Clams from Lombok", softShell.toString());
    }
}