package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BechamelSauceTest {

    private BechamelSauce bechamelSauce;

    @Before
    public void setUp() {
        bechamelSauce = new BechamelSauce();
    }

    @Test
    public void testToString() {
        assertEquals("Bechamel Sauce", bechamelSauce.toString());
    }
}