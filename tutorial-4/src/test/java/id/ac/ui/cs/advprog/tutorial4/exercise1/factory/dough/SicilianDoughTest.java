package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SicilianDoughTest {
    private SicilianStyleDough sicilianDough;

    @Before
    public void setUp(){
        sicilianDough = new SicilianStyleDough();
    }


    @Test
    public void testToString() {
        assertEquals("Sicilian Style Dough", sicilianDough.toString());
    }
}
