package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GoudaCheeseTest {

    private GoudaCheese goudaCheese;

    @Before
    public void setUp() {
        goudaCheese = new GoudaCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Shredded Gouda", goudaCheese.toString());
    }
}
