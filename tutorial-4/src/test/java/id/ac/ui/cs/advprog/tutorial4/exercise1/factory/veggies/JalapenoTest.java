package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class JalapenoTest {

    private Jalapeno jalapeno;

    @Before
    public void setUp() {
        jalapeno = new Jalapeno();
    }

    @Test
    public void testToString() {
        assertEquals("Jalapeno", jalapeno.toString());
    }
}